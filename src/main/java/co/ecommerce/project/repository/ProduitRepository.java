package co.ecommerce.project.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Produit;
import co.ecommerce.project.entity.User;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Integer> {

    @Query("SELECT p FROM Produit p WHERE  p.name LIKE %?1% OR p.marque LIKE %?1%")
    List<Produit> findSearch(String search);

    @Query("SELECT p FROM Produit p WHERE  p.name LIKE %?1% OR p.marque LIKE %?1%")
    Page<Produit> search(String term, Pageable pageable);


    List<Produit> findAllByLikesContaining(User user);
    
    List<Produit> findByGenre(String genre);


    @Query("SELECT p FROM Produit p WHERE p.genre = :genre AND p.categorie.id = :categorieId")
    List<Produit> findByGenreAndCategorie(@Param("genre") String genre, @Param("categorieId") Integer categorieId);
}