package co.ecommerce.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Picture;
import co.ecommerce.project.entity.Produit;

import java.util.List;


@Repository
public interface PictureRepository extends JpaRepository<Picture, Integer>{
    List<Picture> findByProduit(Produit produit);
}
