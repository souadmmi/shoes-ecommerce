package co.ecommerce.project.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Produit;
import co.ecommerce.project.entity.Taille;
import java.util.List;


@Repository
public interface TailleRepository extends JpaRepository<Taille, Integer> {
    List<Taille> findByProduit(Produit produit);
}