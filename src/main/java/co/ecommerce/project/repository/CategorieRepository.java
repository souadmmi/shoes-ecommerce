package co.ecommerce.project.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Categorie;


@Repository
public interface CategorieRepository extends JpaRepository<Categorie, Integer> {

}