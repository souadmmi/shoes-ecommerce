package co.ecommerce.project.entity;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Commande {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
private LocalDateTime date;
private Integer prix;
@OneToMany (mappedBy = "commande", cascade = CascadeType.REMOVE)
private List<LignePanier> panier = new ArrayList<>();
@ManyToOne
private User user;

public Commande(LocalDateTime date, Integer prix, List<LignePanier> panier, User user) {
    this.date = date;
    this.prix = prix;
    this.panier = panier;
    this.user = user;
}
public Commande(Integer id, LocalDateTime date, Integer prix, List<LignePanier> panier, User user) {
    this.id = id;
    this.date = date;
    this.prix = prix;
    this.panier = panier;
    this.user = user;
}
public Commande() {
}
public Integer getId() {
    return id;
}
public void setId(Integer id) {
    this.id = id;
}
public LocalDateTime getDate() {
    return date;
}
public void setDate(LocalDateTime date) {
    this.date = date;
}
public Integer getPrix() {
    return prix;
}
public void setPrix(Integer prix) {
    this.prix = prix;
}
public List<LignePanier> getPanier() {
    return panier;
}
public void setPanier(List<LignePanier> panier) {
    this.panier = panier;
}
public User getUser() {
    return user;
}
public void setUser(User user) {
    this.user = user;
}
}
