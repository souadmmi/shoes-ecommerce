package co.ecommerce.project.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.GenerationType;

@Entity
public class Taille {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer taille;
    private Integer stock;
    @OneToMany(mappedBy = "taille", cascade = CascadeType.REMOVE)
    private List<LignePanier> panier = new ArrayList<>();
    @JsonIgnore
    @ManyToOne
    private Produit produit;

    public Taille(Integer taille, Integer stock, List<LignePanier> panier, Produit produit) {
        this.taille = taille;
        this.stock = stock;
        this.panier = panier;
        this.produit = produit;
    }

    public Taille(Integer id, Integer taille, Integer stock, List<LignePanier> panier, Produit produit) {
        this.id = id;
        this.taille = taille;
        this.stock = stock;
        this.panier = panier;
        this.produit = produit;
    }

    public Taille() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaille() {
        return taille;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public List<LignePanier> getPanier() {
        return panier;
    }

    public void setPanier(List<LignePanier> panier) {
        this.panier = panier;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}
