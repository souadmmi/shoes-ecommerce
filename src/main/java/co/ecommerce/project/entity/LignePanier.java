package co.ecommerce.project.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;

@Entity
public class LignePanier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private Integer quantity;
    private Integer prix;
    @ManyToOne
    private  Commande commande;
    @ManyToOne
    private Taille taille;
    @ManyToOne
    private User user;

    
    public LignePanier(@NotBlank Integer quantity, Integer prix, Taille taille, Commande commande, User user) {
        this.quantity = quantity;
        this.prix = prix;
        this.taille = taille;
        this.commande = commande;
    }
    public LignePanier() {
    }
    public LignePanier(Integer id, @NotBlank Integer quantity, Integer prix, Taille taille, Commande commande, User user) {
        this.id = id;
        this.quantity = quantity;
        this.prix = prix;
        this.taille = taille;
        this.commande = commande;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Integer getPrix() {
        return prix;
    }
    public void setPrix(Integer prix) {
        this.prix = prix;
    }
    public Taille getTaille() {
        return taille;
    }
    public void setTaille(Taille taille) {
        this.taille = taille;
    }
    public Commande getCommande() {
        return commande;
    }
    public void setCommande(Commande commande) {
        this.commande = commande;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
