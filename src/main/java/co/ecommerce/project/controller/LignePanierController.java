package co.ecommerce.project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.LignePanier;
import co.ecommerce.project.entity.Taille;
import co.ecommerce.project.entity.User;
import co.ecommerce.project.repository.LignePanierRepository;

@RestController
@RequestMapping("/api/lignePanier")
public class LignePanierController {

    @Autowired
    private LignePanierRepository repo;

    @Autowired
    private TailleController tailleRepo;

    @GetMapping
    public List<LignePanier> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public LignePanier one(@PathVariable int id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "LignePanier not found"));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LignePanier add(@Validated @RequestBody LignePanier lignePanier) {
        return repo.save(lignePanier);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        LignePanier lignePanier = one(id);
        repo.delete(lignePanier);
    }

    @PutMapping("/{id}")
    public LignePanier update(@PathVariable int id, @Validated @RequestBody LignePanier lignePanier) {
        LignePanier toUpdate = one(id);
        toUpdate.setQuantity(lignePanier.getQuantity());
        toUpdate.setPrix(lignePanier.getPrix());
        toUpdate.setTaille(lignePanier.getTaille());
        toUpdate.setCommande(lignePanier.getCommande());
        return repo.save(toUpdate);
    }

    @PostMapping("/panier/{id}")
    public LignePanier persistPanier(@PathVariable int id, @AuthenticationPrincipal User user) {
        Taille taille = tailleRepo.one(id);
        // regarder si cette taille existe deja dans un panier si oui juste augmenter la
        // quantite si non creer une lignepanier
        LignePanier example = new LignePanier();
        example.setTaille(taille);
        example.setUser(user);
        Optional<LignePanier> panier = repo.findOne(Example.of(example));
        if ( panier.isPresent()) {
            Integer quantity = example.getQuantity() +1;
            //prix du panier avec le nouveau produit ??
            // Integer total = example.getPrix() + example.getTaille().getProduit().getPrix();
            example.setQuantity(quantity);
}
        LignePanier newpanier = new LignePanier();
        newpanier.setQuantity(1);
        newpanier.setPrix(newpanier.getTaille().getProduit().getPrix());
        newpanier.setUser(user);
        repo.save(newpanier);
        return newpanier;
    }

}
