package co.ecommerce.project.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.Note;
import co.ecommerce.project.entity.Produit;
import co.ecommerce.project.entity.User;
import co.ecommerce.project.repository.ProduitRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/produit")
public class ProduitController {

    @Autowired
    private ProduitRepository repo;


    @GetMapping
    public Page<Produit> all(@RequestParam(required = false) String search, 
                            @RequestParam(defaultValue = "1") int pageNumber, 
                            @RequestParam(defaultValue = "5") int pageSize) {
        if(pageSize > 30){
            pageSize = 30;
        }
        Pageable page = Pageable.ofSize(pageSize).withPage(pageNumber);
        if(search != null) {
            return repo.search(search,page);
        }
        return repo.findAll(page);
    }

    @GetMapping("/{id}")
    public Produit one(@PathVariable int id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping("genre/{genre}")
    public List<Produit> ofGender(@PathVariable String genre){
        return repo.findByGenre(genre);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produit add(@Valid @RequestBody Produit produit, @AuthenticationPrincipal User user) {

        repo.save(produit);
        return produit;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        repo.delete(one(id));
    }

    @PutMapping("/{id}")
    public Produit update (@PathVariable int id, @Valid @RequestBody Produit produit, @AuthenticationPrincipal User user) {
        Produit toUpdate = one(id);
        if(user.getRole() != "ROLE_ADMIN" ||user.getRole() != "ROLE_SUPER_ADMIN"){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        toUpdate.setName(produit.getName());
        toUpdate.setDescription(produit.getDescription());
        toUpdate.setImage(produit.getImage());
        toUpdate.setPrix(produit.getPrix());
        toUpdate.setMarque(produit.getMarque());
        toUpdate.setCategorie(produit.getCategorie());
        repo.save(toUpdate);
        return toUpdate; 
    
}
    @GetMapping("/{id}/note")
        public  List<Note> getNotes(@PathVariable int id) {
        Produit produit = one(id);
        return produit.getNotes();
    }

    @GetMapping("/{genre}/{categorieId}")
    public List<Produit> getProduitsByGenreAndCategorie(
            @PathVariable String genre,
            @PathVariable Integer categorieId) {
        return repo.findByGenreAndCategorie(genre, categorieId);
    }


    @PatchMapping("/{id}/like")
    public Produit like(@PathVariable int id, @AuthenticationPrincipal User user) {
        Produit produit = one(id);
        if(!produit.getLikes().removeIf(item -> item.getId().equals(user.getId()))) {
            produit.getLikes().add(user);
        }
        repo.save(produit);
        return produit;
    }

    @GetMapping("/userLike")
    public List<Produit> getLikeOfUser(@AuthenticationPrincipal User user) {
        return repo.findAllByLikesContaining(user);
}


    @GetMapping("/search/{search}")
    public List<Produit>searchProduct(@PathVariable String search){
        return repo.findSearch(search);
}



}