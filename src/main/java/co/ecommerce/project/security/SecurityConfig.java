package co.ecommerce.project.security;


import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class SecurityConfig {

    
    @Value("${frontend.url}")
    private String frontUrl;
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http)  throws Exception {

        http.httpBasic(Customizer.withDefaults())
        .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED))
        .cors(cors -> cors.configurationSource(corsConfigurationSource()))
        .csrf(csrf -> csrf.disable())
        .logout(Customizer.withDefaults());

        http.authorizeHttpRequests(request -> 
            request.requestMatchers(HttpMethod.GET, "/api/account", "/api/user/{id}", "/api/produit/userLike").authenticated()
            .requestMatchers(HttpMethod.POST, "/api/note/**").authenticated()
            .requestMatchers(HttpMethod.PUT, "/api/note/**").authenticated()
            .requestMatchers(HttpMethod.DELETE, "/api/note/**").authenticated()
            .requestMatchers(HttpMethod.DELETE, "/api/produit/**").hasAnyRole("ADMIN", "SUPER_ADMIN")
            .requestMatchers(HttpMethod.PATCH, "/api/produit").hasAnyRole("ADMIN", "SUPER_ADMIN")
            .requestMatchers(HttpMethod.POST, "/api/produit/**").hasAnyRole("ADMIN", "SUPER_ADMIN")
            .anyRequest().permitAll()
        );

        return http.build();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);

    }

    private CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(Arrays.asList(frontUrl));
        configuration.setAllowedMethods(Arrays.asList("*"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


}

