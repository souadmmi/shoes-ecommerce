-- Active: 1709545609475@@127.0.0.1@3306@ecommerce


INSERT INTO user (email, name, password,role) VALUES
('superadmin@test.com', "adminsuper",  '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_SUPER_ADMIN'),
('admin@test.com', "admin", '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('test@test.com', "test" , '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER'),
('souad@test.com', "souad" , '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER'),
('aymeric@test.com', "aymeric" , '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER'),
('romane@test.com', "romane" , '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');

INSERT INTO categorie (name) VALUES 
("Chaussure de ville"),
("Elevation Elegante"),
("Rythme Épique"),
("Claquette chaussettes");


INSERT INTO produit (genre,name, prix, marque, categorie_id, image, description) VALUES
("Femme", "Baskets - Chuck Taylor - Blanc", 95.00, "Converse", 1, 'http://localhost:8080/converseblanche8.png', "Les baskets Chuck Taylor Lift Hi de la marque Converse sont des incontournables du dressing féminin. Avec leur dessus en textile, leur doublure en textile et leur fermeture à lacets, ces baskets offrent un style intemporel et décontracté. Dotées d'une semelle extérieure en caoutchouc et d'une semelle intérieure en textile, elles garantissent confort et adhérence."),
("Femme","Escarpins - 22433-41 - Argent", 56.00, "Tamaris", 2, "http://localhost:8080/tamarin1.png", "Ces escarpins Tamaris - modèle 22433-41 - incarnent l'élégance et la sophistication avec une touche contemporaine. Leur couleur argentée les rend captivants, ajoutant une dose de glamour à n'importe quelle tenue. Fabriqués avec soin et attention aux détails, ces escarpins offrent un mariage parfait entre style et confort.Leur silhouette classique est rehaussée par des lignes épurées et des finitions impeccables, soulignant la qualité de leur conception. La hauteur de leur talon est idéale pour ajouter de la hauteur sans sacrifier le confort, vous permettant de rester élégante tout au long de la journée ou de la soirée. Que ce soit pour une occasion spéciale, une soirée entre amis ou une journée au bureau, ces escarpins argentés Tamaris sont le choix parfait pour celles qui recherchent à la fois style, confort et sophistication. Avec eux, chaque pas devient une déclaration de mode."),
("Femme", "Escarpins - 22412-41 - Noir", 63.00, "Marco Tozzi", 2, "http://localhost:8080/tozzi1.png", "Les escarpins Marco Tozzi, modèle 22412-41, sont l'incarnation du chic intemporel et de l'élégance classique. Leur couleur noire classique en fait des pièces polyvalentes qui s'harmonisent parfaitement avec toutes les tenues, qu'elles soient professionnelles ou élégantes. Conçus avec un souci du détail et une expertise artisanale, ces escarpins offrent une silhouette raffinée et des lignes épurées qui ajoutent une touche de sophistication à chaque pas. La qualité de leur fabrication assure un confort optimal, vous permettant de vous sentir à l'aise tout au long de la journée ou de la soirée. Dotés d'un talon élégant qui ajoute juste ce qu'il faut de hauteur, ces escarpins sont à la fois élégants et pratiques, parfaits pour toutes les occasions, qu'il s'agisse d'une réunion importante au bureau ou d'une soirée spéciale entre amis. Avec les escarpins Marco Tozzi 22412-41, vous êtes prête à affronter le monde avec style et confiance."),
("Homme", "Baskets - MR530 M - Blanc", 126.00, "New Balance", 1, "http://localhost:8080/new1.png", "Les baskets New Balance MR530 M pour homme sont des incontournables du dressing, alliant style et confort. Avec leur dessus en textile et synthétique, leur doublure en textile et leur fermeture à lacets, ces baskets offrent un look emblématique et décontracté. La marque New Balance, reconnue pour son design reconnaissable et son engagement envers la qualité, propose un modèle lifestyle parfait pour le quotidien."),
("Homme", "Baskets - U327 M - Orange", 150.00, "New Balance", 1, "http://localhost:8080/newbalance1.png", "Les baskets New Balance U327 M sont des incontournables du dressing masculin. Avec leur dessus en cuir et textile, leur doublure en textile et leur fermeture à lacets, ces baskets allient style et confort. La marque emblématique New Balance est reconnue pour son design reconnaissable et son engagement envers la qualité et l'innovation. Parfaites pour le quotidien, ces baskets lifestyle offrent un équilibre optimal entre esthétique, style et technologies de pointe."),
("Homme", "Basket - U327 M - Marron ", 135.00, "New Balance", 1 , "http://localhost:8080/newbalancemarron1.png", "Les baskets New Balance U327 M sont des incontournables du dressing masculin. Avec leur dessus en cuir et textile, leur doublure en textile et leur fermeture à lacets, ces baskets allient style et confort. La marque emblématique New Balance est reconnue pour son design reconnaissable et son engagement envers la qualité et l'innovation. Parfaites pour le quotidien, ces baskets lifestyle offrent un équilibre optimal entre esthétique, style et technologies de pointe.");


INSERT INTO taille (taille,stock, produit_id) VALUES
(37,100, 1),
(38,100, 1),
(39,100, 1),
(40,100, 1),
(41,100, 1),
(42,100, 1),
(37,100, 2),
(38,100, 2),
(39,100, 2),
(40,100, 2),
(41,100, 2),
(42,100, 2),
(37,100, 3),
(38,100, 3),
(39,100, 3),
(40,100, 3),
(41,100, 3),
(42,100, 3),
(37,100, 4),
(38,100, 4),
(39,100, 4),
(40,100, 4),
(41,100, 4),
(42,100, 4);

-- INSERT INTO commande (date, prix, user_id) VALUES
-- ("2020-01-01",100, 2, 1);

-- INSERT INTO ligne_panier (quantity,prix, commande_id, taille_id) VALUES
-- (1,100, 1, 1);




INSERT INTO note (content,date, author_id, produit_id) VALUES
("Bon produit","2020-01-01", 1, 1);



INSERT INTO picture (url, produit_id) VALUES 
('http://localhost:8080/converseblanche1.png', 1),
('http://localhost:8080/converseblanche2.png', 1),
('http://localhost:8080/converseblanche3.png', 1),
('http://localhost:8080/converseblanche6.png', 1),
('http://localhost:8080/converseblanche7.png', 1),
('http://localhost:8080/converseblanche8.png', 1),
('http://localhost:8080/converseblanche9.png', 1),
("http://localhost:8080/tamarin2.png", 2),
("http://localhost:8080/tamarin3.png", 2),
("http://localhost:8080/tamarin4.png", 2),
("http://localhost:8080/tamarin5.png", 2),
("http://localhost:8080/tamarin6.png", 2),
("http://localhost:8080/tozzi6.png", 3),
("http://localhost:8080/tozzi2.png", 3),
("http://localhost:8080/tozzi3.png", 3),
("http://localhost:8080/tozzi4.png", 3),
("http://localhost:8080/tozzi5.png", 3),
("http://localhost:8080/new2.png", 4),
("http://localhost:8080/new3.png", 4),
("http://localhost:8080/new4.png", 4),
("http://localhost:8080/new5.png", 4),
("http://localhost:8080/new6.png", 4),
("http://localhost:8080/newbalance2.png", 5),
("http://localhost:8080/newbalance3.png", 5),
("http://localhost:8080/newbalance4.png", 5),
("http://localhost:8080/newbalance5.png", 5),
("http://localhost:8080/newbalance6.png", 5),
("http://localhost:8080/newbalancemarron2.png", 6),
("http://localhost:8080/newbalancemarron3.png", 6),
("http://localhost:8080/newbalancemarron4.png", 6),
("http://localhost:8080/newbalancemarron5.png", 6),
("http://localhost:8080/newbalancemarron6.png", 6);


-- SELECT * FROM produit WHERE genre=femme AND categorie_id=1;

SELECT * FROM produit WHERE genre="Femme" AND categorie_id=1;